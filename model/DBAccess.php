<?php
    class DBAccess
    {
        private $db;
        public $params = array();

        public function __construct()
        {
            $this->connect();
        }


        public function connect()
        {
            try {
                $this->db = new PDO('mysql:host=localhost;dbname=oprmars;charset=utf8mb4', 'root', '',
                array(PDO::ATTR_EMULATE_PREPARES => false,
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            } catch (PDOException $error) {
                echo $error->getMessage();
                die("Database connection error");
            }
        }

        public function addParam($paramname, $param)
        {
            $this->params[$paramname] = addslashes(strip_tags(trim($param)));
        }

        public function query($sqlString)
        {
            try{
                if (!$this->params) {
                $this->queryString = $this->db->query($sqlString);
            } else {
                $this->queryString = $this->db->prepare($sqlString);
                $this->queryString->execute($this->params);
            }

            if ($this->params) {
                reset($this->params);
                unset($this->params);
            }

            return $this->queryString->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $queryOnee) {
                return $queryOnee;
            }
            
        }

        public function queryOne($sqlString)
        {
            try{
                if (!$this->params) {
                    $this->queryString = $this->db->query($sqlString);
                } else {
                    $this->queryString = $this->db->prepare($sqlString);
                    $this->queryString->execute($this->params);
                }

                if ($this->params) {
                    reset($this->params);
                    unset($this->params);
                }
            
                return $this->queryString->fetch(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                return $e;
            }
            
        }

        public function getParams()
        {
            return $this->params;
        }
    }

?>