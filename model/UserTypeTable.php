<?php
class UserTypeTable
{
	private $dbConnection;

	public function __construct()
	{
		$this->dbConnection = new DBAccess();
	}

	public function getUserTypes()
	{
		$sqlCommand = "SELECT * FROM user_types";
		$results =$this->dbConnection->query($sqlCommand);

		if ($results) {
			return $results;
		}

		return false;
	}

	public function getUserType($user_type_code)
	{
		$sqlCommand = "SELECT * FROM user_types WHERE user_type_code = :user_type_code";
		$this->dbConnection->addParam(':user_type_code', $user_type_code);
		$results =$this->dbConnection->queryOne($sqlCommand);

		if ($results) {
			return $results;
		}

		return false;
	}

	public function update($data, $user_id)
    {
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $this->dbConnection->addParam(':'.$key, $value);
            }

            $this->dbConnection->addParam('user_id', $user_id);

            $sqlCommand = "UPDATE users SET {$key} = :{$key} WHERE user_id = :user_id";
            $results = $this->dbConnection->query($sqlCommand);

            if ($results) {
                return $results;
            }

            return false;

        }
    }
}