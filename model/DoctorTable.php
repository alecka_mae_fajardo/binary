<?php
class DoctorTable
{
	private $dbConnection;

	public function __construct()
	{
		$this->dbConnection = new DBAccess();
	}

	public function getDoctors()
	{
		
		$sqlCommand = "SELECT * FROM doctors";
		$results =$this->dbConnection->query($sqlCommand);

		if ($results) {
			return $results;
		}

		return false;
	}

	public function getDoctor($doctor_id)
	{
		$this->dbConnection->addParam(':doctor_id', $doctor_id);
		$sqlCommand = "SELECT * FROM doctors WHERE doctor_id = :doctor_id";
		$results =$this->dbConnection->queryOne($sqlCommand);

		if ($results) {
			return $results;
		}

		return false;
	}

	public function addDoctor($doctorInfo)
	{
		if (!empty($doctorInfo)) {
			foreach ($doctorInfo as $key => $value) {
				if ($key != 'btn_sup') {
					$this->dbConnection->addParam(':'.$key, $value);
				}
			}
			$sqlCommand = "INSERT INTO doctors (first_name, last_name, middle_name, specialization, workplace, email) 
							VALUES (:first_name, :last_name, :middle_name, :specialization, :workplace, :email)";
			$results = $this->dbConnection->query($sqlCommand);
		}
	}

	public function editDoctor($doctorInfo)
	{
		if (!empty($doctorInfo)) {
			foreach ($doctorInfo as $key => $value) {
				if ($key != 'btn_sup') {
					$this->dbConnection->addParam(':'.$key, $value);
				}
			}
			$sqlCommand = "UPDATE doctors SET first_name = :first_name, last_name = :last_name, middle_name = :middle_name, specialization = :specialization, workplace = :workplace, email = :email WHERE doctor_id = :doctor_id";
			$results = $this->dbConnection->query($sqlCommand);
		}
	}

	public function deleteDoctor($doctor_id)
	{
		$this->dbConnection->addParam(':doctor_id', $doctor_id);
		$sqlCommand = "DELETE FROM doctors WHERE doctor_id = :doctor_id";
		$results = $this->dbConnection->query($sqlCommand);

		return true;
	}

	public function getAvailableDoctor()
    {
        $sqlCommand = "SELECT d.first_name, d.last_name, ds.time, ds.day FROM `doctor_schedules` ds 
                       JOIN doctors d ON d.doctor_id = ds.doctor_id
                        WHERE ds.day = DAYNAME(curdate())";
        $results =$this->dbConnection->query($sqlCommand);

        if ($results) {
            return $results;
        }

        return false;
    }
}