<?php
class PatientTable
{
	private $dbConnection;

	public function __construct()
	{
		$this->dbConnection = new DBAccess();
	}

	public function getPatients()
	{
		$sqlCommand = "SELECT * FROM patients";
		$results =$this->dbConnection->query($sqlCommand);

		if ($results) {
			return $results;
		}

		return false;
	}

	public function getPatient($patient_id)
	{
		$this->dbConnection->addParam(':patient_id', $patient_id);
		$sqlCommand = "SELECT * FROM patients WHERE patient_id = :patient_id";
		$results =$this->dbConnection->queryOne($sqlCommand);

		if ($results) {
			return $results;
		}

		return false;
	}

	public function deletePatient($patient_id)
	{
		$this->dbConnection->addParam(':patient_id', $patient_id);
		$sqlCommand = "DELETE FROM patients WHERE patient_id = :patient_id";
		$results = $this->dbConnection->query($sqlCommand);

		return true;
	}

	public function addPatient($patientInfo)
	{
		if (!empty($patientInfo)) {
			foreach ($patientInfo as $key => $value) {
				if ($key != 'btn_sup') {
					$this->dbConnection->addParam(':'.$key, $value);
				}
			}

			$sqlCommand = "INSERT INTO patients (first_name, last_name, middle_name, email, phone_no, address, gender, birthdate, birthplace, contact_person, contact_person_no, weight, height, bp, blood_type, brgy) VALUES (:first_name, :last_name, :middle_name, :email, :phone_no, :address, :gender, :birthdate, :birthplace, :contact_person, :contact_person_no, :weight, :height, :bp, :blood_type, :brgy)";
			$results = $this->dbConnection->query($sqlCommand);
		}
	}

	public function editPatient($patientInfo)
	{
		if (!empty($patientInfo)) {
			foreach ($patientInfo as $key => $value) {
				
				$this->dbConnection->addParam(':'.$key, $value);
			}

			

			$sqlCommand = "UPDATE patients SET first_name = :first_name, last_name = :last_name, middle_name = :middle_name, email = :email, phone_no = :phone_no, address = :address, gender = :gender, birthdate = :birthdate, birthplace = :birthplace, contact_person = :contact_person, contact_person_no = :contact_person_no, weight = :weight, height = :height, bp = :bp, blood_type = :blood_type, brgy = :brgy WHERE patient_id = :patient_id";
			$results = $this->dbConnection->query($sqlCommand);

			
		}
	}

	public function countPatients()
	{
		$sqlCommand = "SELECT count(*) as count FROM patients";
		$results =$this->dbConnection->queryOne($sqlCommand);

		if ($results) {
			return $results->count;
		}

		return false;
	}
}

/**

insert query
$this->dbConnection->addParam(':patient_id', $patient_id);

*/