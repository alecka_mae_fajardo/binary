<?php
class EventTable
{
	private $dbConnection;

	public function __construct()
	{
		$this->dbConnection = new DBAccess();
	}

	public function getEvents()
	{
		$sqlCommand = "SELECT * FROM events";
		$results =$this->dbConnection->query($sqlCommand);

		if ($results) {
			return $results;
		}

		return false;
	}

	public function getEvent($patient_id)
	{
		$this->dbConnection->addParam(':event_id', $event_id);
		$sqlCommand = "SELECT * FROM events WHERE event_id = :event_id";
		$results =$this->dbConnection->queryOne($sqlCommand);

		if ($results) {
			return $results;
		}

		return false;
	}

	public function deleteEvent($event_id)
	{
		$this->dbConnection->addParam(':event_id', $event_id);
		$sqlCommand = "DELETE FROM events WHERE event_id = :event_id";
		$results = $this->dbConnection->query($sqlCommand);

		return true;
	}

	public function addEvent($eventInfo)
	{
		if (!empty($eventInfo)) {
			foreach ($eventInfo as $key => $value) {
				if ($key != 'btn_submit') {
					$this->dbConnection->addParam(':'.$key, $value);
				}
			}

			$sqlCommand = "INSERT INTO `events` (`event_name`, `event_date`, `event_venue`, `event_details`, `image`) 
							VALUES (:event_name, :event_date, :event_venue, :event_details)";
			$results = $this->dbConnection->query($sqlCommand);
		}
	}

	public function editEvent($eventInfo)
	{
		if (!empty($eventInfo)) {
			foreach ($eventInfo as $key => $value) {
				
				$this->dbConnection->addParam(':'.$key, $value);
			}

			

			$sqlCommand = "UPDATE events SET event_name = :event_name, event_date = :event_date, event_venue = :event_venue, event_details = :event_details, image = :image";
			$results = $this->dbConnection->query($sqlCommand);

			
		}
	}
}

/**

insert query
$this->dbConnection->addParam(':patient_id', $patient_id);

*/