<?php
class RequestsTable
{
	private $dbConnection;

	public function __construct()
	{
		$this->dbConnection = new DBAccess();
	}

	public function getRequests()
	{
		$sqlCommand = "SELECT r.*, CONCAT(p.first_name, ' ' , p.last_name) as patient,
		               CONCAT(d.first_name, ' ' , d.last_name) as doctor
                        FROM requests r
                        JOIN patients p on p.patient_id = r.patient_id
                        JOIN doctors d on d.doctor_id = r.doctor_id
                        WHERE r.delete_flag = 0";
		$results =$this->dbConnection->query($sqlCommand);

		if ($results) {
			return $results;
		}

		return false;
	}

	public function getRequest($request_id)
	{
		$this->dbConnection->addParam(':request_id', $request_id);
		$sqlCommand = "SELECT * FROM requests WHERE request_id = :request_id";
		$results =$this->dbConnection->queryOne($sqlCommand);

		if ($results) {
			return $results;
		}

		return false;
	}

	public function getLatestRequests()
	{
		$sqlCommand = "SELECT r.request_id, CONCAT(p.first_name, ' ', p.last_name) as patientName, NOW(), 
						CONCAT(d.first_name, ' ', d.last_name) as doctorName, r.health_concern, r.status
						FROM requests r 
						JOIN patients p ON r.patient_id = p.patient_id
						JOIN doctors d ON r.doctor_id = d.doctor_id
						WHERE r.request_datetime  >= CURDATE()";

		$results =$this->dbConnection->query($sqlCommand);

		if ($results) {
			return $results;
		}

		return false;
	}

	public function countRequests()
	{
		$sqlCommand = "SELECT count(*) as count FROM requests";
		$results =$this->dbConnection->queryOne($sqlCommand);

		if ($results) {
			return $results->count;
		}

		return false;
	}


    public function addRequest($requestInfo)
    {
        if (!empty($requestInfo)) {
            $requestInfo['request_id'] = NULL;
            foreach ($requestInfo as $key => $value) {
                if ($key != 'btn_sup') {
                    $this->dbConnection->addParam(':'.$key, $value);
                }
            }

            $sqlCommand = "INSERT INTO requests (request_id, user_id, patient_id, doctor_id, status, health_concern, request_datetime, request_type, delete_flag) 
                                          VALUES (:request_id, :user_id, :patient_id, :doctor_id, :status, :health_concern, :request_datetime, :request_type, :delete_flag)";

            try {
                $results = $this->dbConnection->query($sqlCommand);
            } catch(Exception $exception) {
                die($exception->getMessage()); exit;
            }

            return $results;
        }
    }
}