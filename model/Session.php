<?php
class Session
{
	public $userKey = 'user';

	public function setUserSession($value)
	{
		$_SESSION[$this->userKey] = $value;
	}	

	public function getUserSession()
	{
		return isset($_SESSION[$this->userKey]) ? $_SESSION[$this->userKey] : null;
	}

	public function unsetSession()
	{
		unset($_SESSION[$this->userKey]);
	}
}