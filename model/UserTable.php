<?php
class UserTable
{
	private $dbConnection;

	public function __construct()
	{
		$this->dbConnection = new DBAccess();
	}

	public function getUsers()
	{
		$sqlCommand = "SELECT * FROM users";
		$results =$this->dbConnection->query($sqlCommand);

		if ($results) {
			return $results;
		}

		return false;
	}

	public function getUserLogin($user_type_code, $username, $password)
	{
		$sqlCommand = "SELECT * FROM users u
			JOIN user_types ut ON u.user_type_code = ut.user_type_code
			WHERE u.user_type_code = :user_type_code 
			AND u.username = :username AND u.password = :password";
		$this->dbConnection->addParam(':user_type_code', $user_type_code);
		$this->dbConnection->addParam(':username', $username);
		$this->dbConnection->addParam(':password', $password);
		$results =$this->dbConnection->queryOne($sqlCommand);

		if ($results) {
			return $results;
		}

		return false;
	}
}

/**

insert query


*/