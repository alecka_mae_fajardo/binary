<?php
class Autoloader
{
    static public function loadClass($className)
    {
        $className = str_replace('\\', '/', $className);
        if(file_exists($className . '.php')) {
            include $className . '.php';
        } else {
            require_once '../model/' . $className . '.php';
        }
    }
}