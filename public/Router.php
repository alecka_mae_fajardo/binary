<?php

class Router
{
    private $routes = array(
        '/' => array(
            'controller'	=> 'UserController',
            'action'		=> 'indexAction'
        ),
        /** user login **/
        '/user-login' => array(
            'controller'	=> 'UserController',
            'action'		=> 'viewUserLoginAction',
            'viewTemplate'  => "view/user/select-user-login.phtml"
        ),
        '/login' => array(
            'controller'	=> 'UserController',
            'action'		=> 'loginAction',
            'viewTemplate'  => "view/user/login.phtml"
        ),
        '/logout' => array(
            'controller'	=> 'UserController',
            'action'		=> 'logoutAction'
        ),
        '/validate-login' => array(
            'controller'	=> 'UserController',
            'action'		=> 'validateLoginAction'
        ),

        /** dashboard of logged in user based on user type code*/
        '/patient' => array(
            'controller'	=> 'PatientController',
            'action'		=> 'viewPatientPageAction',
            'viewTemplate' 	=> "view/patient/page.phtml"
        ),
        '/bhc-doctor' => array(
            'controller'	=> 'DoctorController',
            'action'		=> 'viewDoctorPageAction',
            'viewTemplate' 	=> "view/doctor/dashboard.phtml"
        ),
        '/hospital-doctor' => array(
            'controller' 	=> 'DoctorController',
            'action' 		=> 'viewDoctorPageAction',
            'viewTemplate' 	=> "view/doctor/dashboard.phtml"
        ),
        '/bhc-admin' => array (
            'controller' 	=> 'AdminController',
            'action'		=> 'viewAdminPageAction',
            'viewTemplate'	=> "view/admin/dashboard.phtml"
        ),
        '/hospital-admin' => array (
            'controller' 	=> 'AdminController',
            'action' 		=> 'viewAdminPageAction',
            'viewTemplate'	=> "view/admin/dashboard.phtml"
        ),

        /** patient record management **/
        '/view-patient' => array(
            'controller'	=> 'PatientController',
            'action'		=> 'viewAction',
            'viewTemplate'	=>  'view/patient/view-one.phtml'
        ),
        '/view-patients' => array(
            'controller'	=> 'PatientController',
            'action'		=> 'viewAllAction',
            'viewTemplate'	=>  'view/patient/view-all.phtml'
        ),
        '/add-patient' => array (
            'controller' 	=> 'PatientController',
            'action' 		=> 'viewAddPatientAction',
            'viewTemplate'	=> "view/patient/register-patient.phtml"
        ),
        '/process-register' => array (
            'controller' 	=> 'PatientController',
            'action' 		=> 'registerPatientAction'
        ),
        '/delete-patient' => array (
            'controller' 	=> 'PatientController',
            'action' 		=> 'viewDeletePatientAction',
            'viewTemplate'	=>  'view/patient/view-all.phtml'
        ),
        '/process-edit' => array (
            'controller' 	=> 'PatientController',
            'action' 		=> 'viewEditPatientAction',
            //'viewTemplate'	=>  'view/patient/view-all.phtml'
        ),
        '/view-my-health-record' => array (
            'controller' 	=> 'PatientController',
            'action' 		=> 'viewMyRecordPageAction',
            'viewTemplate'	=> "view/patient/myhealthrecord.phtml"
        ),

        /** event record management **/
        '/view-events' => array (
            'controller' 	=> 'EventController',
            'action' 		=> 'viewEventsAction',
            'viewTemplate'	=>  'view/events/view-all.phtml'
        ),
        '/view-event' => array (
            'controller' 	=> 'EventController',
            'action' 		=> 'viewEventAction',
            'viewTemplate'	=>  'view/events/view-one.phtml'
        ),
        '/add-event' => array (
            'controller' 	=> 'EventController',
            'action' 		=> 'viewAddEventAction',
            'viewTemplate'	=>  'view/events/add-event.phtml'
        ),

        /** appointment reservation request management **/
        '/view-requests' => array (
            'controller' 	=> 'RequestController',
            'action' 		=> 'viewRequestsAction',
            'viewTemplate'	=>  'view/request/view-all.phtml'
        ),
        '/view-request' => array (
            'controller' 	=> 'RequestController',
            'action' 		=> 'viewRequestAction',
            'viewTemplate'	=>  'view/request/view-one.phtml'
        ),
        '/add-request' => array (
            'controller' 	=> 'RequestController',
            'action' 		=> 'addRequestAction',
            'viewTemplate'	=>  'view/request/add-request.phtml'
        ),
        '/process-request' => array (
            'controller' 	=> 'RequestController',
            'action' 		=> 'processRequestAction',
        ),
        '/request-checkup' => array (
            'controller' 	=> 'PatientController',
            'action' 		=> 'viewRequestPageAction',
            'viewTemplate'	=> "view/patient/request-page.phtml"
        ),
        '/view-referrals' => array (
            'controller' 	=> 'PatientController',
            'action' 		=> 'viewMyReferralPageAction',
            'viewTemplate'	=> "view/patient/myreferral.phtml"
        ),

        /** doctor request management **/
        '/view-doctors' => array (
            'controller' 	=> 'DoctorController',
            'action' 		=> 'viewAllAction',
            'viewTemplate'	=> "view/doctor/view-all.phtml"
        ),
        '/view-doctor' => array(
            'controller'	=> 'DoctorController',
            'action'		=> 'viewAction',
            'viewTemplate'	=> "view/doctor/view-one.phtml"
        ),
        '/view-register-doctor' => array (
            'controller' 	=> 'DoctorController',
            'action' 		=> 'viewAddDoctorAction',
            'viewTemplate'	=> "view/doctor/register-doctor.phtml"
        ),
        '/add-doctor' => array (
            'controller' 	=> 'DoctorController',
            'action' 		=> 'registerDoctorAction'
        ),
        '/update-doctor' => array (
            'controller' 	=> 'DoctorController',
            'action' 		=> 'UpdateDoctorAction'
            //'viewTemplate'	=>  'view/doctor/view-all.phtml'
        ),
        '/delete-doctor' => array (
            'controller' 	=> 'DoctorController',
            'action' 		=> 'deleteDoctorAction',
            'viewTemplate'	=>  'view/doctor/view-all.phtml'
        ),
    );

    public function parseAndFindControllerAndActionForRequestUri($uri)
    {
        $serverUrl = explode('?', $uri);
        @list($base_path, $controller, $action) = explode("/", $serverUrl[0], 4);
        $uri = $serverUrl[0];
        $params = isset($uri[1]) ? $uri[1] : '';

        if(isset($this->routes[$uri])) {
            $match['class']  =  $this->routes[$uri]['controller'];
            $match['action'] = $this->routes[$uri]['action'];

            if (isset($this->routes[$uri]['viewTemplate'])) {
                $match['viewTemplate'] = $this->routes[$uri]['viewTemplate'];
            }

            if(isset($this->routes[$uri]['params'])) {
                echo '<div>Params found</div>';
                if ($this->routes[$uri]['params']) {
                    $match['params'] = explode('/', $params);
                }
            }
            return $match;
        } else {
            $match['class']     = $controller;
            $match['action']    = $action;

            if (isset($this->routes[$uri])) {
                if ($this->routes[$uri]['viewTemplate']) {
                    $match['viewTemplate'] = $this->routes[$uri]['viewTemplate'];
                }

                if(isset($params))
                    $match['params'] = explode('/', $params);

                return $match;

            } else {
                return false;
            }
        }
    }
}