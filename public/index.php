<?php
error_reporting(E_ALL);
session_start();
require_once 'Autoloader.php';
define('BASE_URL', 'local.binary.com/');
define('ROOT_DIR', 'binary/');
define('LAYOUT', "../view/template/layout.phtml");
define('LAYOUT2', "../view/template/layout2.phtml");

spl_autoload_register('Autoloader::loadClass');

$request_uri    = trim($_SERVER['REQUEST_URI']);
$router         = new Router();
$route          = $router->parseAndFindControllerAndActionForRequestUri($request_uri);

if (!$route) {
    require_once ('../public/404.php');
} else {
    $dispatcher     = new Dispatcher($route);
    $isDispatched   = $dispatcher->dispatch();

    if (!$isDispatched) {
        echo $route['class'] .'/' . $route['action'] . ' not found';
        require_once ('../public/404.php');
    }
}
