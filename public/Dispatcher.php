<?php

class Dispatcher
{
    private $route;

    public function __construct($route)
    {
        $this->route = $route;
    }

    public function dispatch()
    {
        $class  = isset($this->route['class']) ? $this->route['class'] : '';
        $action = isset($this->route['action']) ? $this->route['action'] : '';
        $params = isset($this->route['params']) ? $this->route['params'] : [];
        $params['viewTemplate'] = isset($this->route['viewTemplate']) ? $this->route['viewTemplate'] : [];

        include '../controller/' . $class . '.php';

        if(class_exists($class)) {
            if(method_exists($class, $action)) {

                $Controller = new $class();
                if (!empty($params['viewTemplate'])) {
                    $Controller->viewTemplate = '../' . $params['viewTemplate'];
                }
                $Controller->$action();

                //call_user_func_array(array(new $class, $action), $params);
                return true;
            }
        }
        return false;
    }
}