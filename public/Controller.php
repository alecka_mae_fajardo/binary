<?php
/**
 * Created by PhpStorm.
 * User: camille.v
 * Date: 11/30/2017
 * Time: 9:57 AM
 */

namespace core;

use libs\filters\InputFilter;
use libs\filters\InputSanitizer;
use libs\Template;

class Controller
{
    protected $template;
    protected $inputFilter;
    protected $inputSanitizer;

    public function __construct()
    {
        $this->template         = new Template();
        $this->inputFilter      = new InputFilter();
        $this->inputSanitizer   = new InputSanitizer();
    }
}