-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2018 at 09:24 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oprmars`
--

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `doctor_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `specialization` varchar(50) NOT NULL,
  `workplace` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`doctor_id`, `first_name`, `last_name`, `middle_name`, `specialization`, `workplace`, `email`) VALUES
(1, 'Tyrone', 'Fermines', 'Pangilinan', 'Optimologist', 'Hospital', 'tyrone.fermin@gmail.com'),
(2, 'Justin', 'Mendoza', 'Dela Cruz', 'Cardiologist', 'Barangay Health Center', 'justin.mendoza@yahoo.com'),
(3, 'Rogelio', 'Almaden', 'Arandela', 'Pedia', 'Barangay Health Center', 'rogelio_almaden@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_schedules`
--

CREATE TABLE `doctor_schedules` (
  `doctor_id` int(11) NOT NULL,
  `day` varchar(50) NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `event_name` varchar(100) NOT NULL,
  `event_date` datetime NOT NULL,
  `event_venue` varchar(100) NOT NULL,
  `event_details` text NOT NULL,
  `workplace` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`event_id`, `user_id`, `event_name`, `event_date`, `event_venue`, `event_details`, `workplace`) VALUES
(0, 1, 'Test', '2017-12-28 00:00:00', 'test', 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `patient_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone_no` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `birthdate` date NOT NULL,
  `birthplace` varchar(50) NOT NULL,
  `contact_person` varchar(50) NOT NULL,
  `contact_person_no` varchar(50) NOT NULL,
  `weight` varchar(50) NOT NULL,
  `height` varchar(50) NOT NULL,
  `bp` varchar(50) NOT NULL,
  `blood_type` varchar(50) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `brgy` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`patient_id`, `first_name`, `last_name`, `middle_name`, `email`, `phone_no`, `address`, `gender`, `birthdate`, `birthplace`, `contact_person`, `contact_person_no`, `weight`, `height`, `bp`, `blood_type`, `active`, `brgy`) VALUES
(10, 'Denrik', 'Almaden', 'Matienzo', 'jdenalmaden@gmail.com', '09264764885', 'Hulo, Mandaluyong City', 'Male', '1996-09-07', 'San Pablo', 'Rogelio Almaden', '0912345678', '65', '78', '82', 'AB', 1, 'Addition Hillss'),
(11, 'Tyrone', 'Fermin', '', '', '0912345678', '', 'Male', '1998-12-28', '', '', '', '', '', '', '-- Select --', 1, 'Addition Hills'),
(12, 'Antonio', 'Valdez', '', '', '0987654321', '', 'Male', '1993-00-00', '', '', '', '', '', '', '-- Select --', 1, 'Addition Hills'),
(13, 'Alecka Mae', 'Fajardo', '', '', '09453904537', '', '-- Select --', '1993-12-28', '', '', '', '', '', '', '-- Select --', 1, 'Addition HIlls'),
(15, 'Kimberly', 'Caguicla', 'Molina', 'kim_caguicla@gmail.com', '09453904537', 'knkn', 'Female', '1997-12-31', 'San Pablo, Laguna', 'asa', '09265448679', '87', '79', '82', 'AB', 1, 'Hulo');

-- --------------------------------------------------------

--
-- Table structure for table `patients_history`
--

CREATE TABLE `patients_history` (
  `patient_history_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `history_datetime` datetime NOT NULL,
  `history_info` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `patient_prescriptions`
--

CREATE TABLE `patient_prescriptions` (
  `patient_presc_id` int(11) NOT NULL,
  `patient_record_id` int(11) NOT NULL,
  `generic_name` varchar(100) NOT NULL,
  `dosage` varchar(100) NOT NULL,
  `brand_name` varchar(100) NOT NULL,
  `frequency` varchar(100) NOT NULL,
  `quantity/route` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `patient_records`
--

CREATE TABLE `patient_records` (
  `patient_record_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `patient_history_id` int(11) NOT NULL,
  `date_recorded` date NOT NULL,
  `results` text NOT NULL,
  `diagnosis` text NOT NULL,
  `complaints` text NOT NULL,
  `illness` text NOT NULL,
  `generic_name` text NOT NULL,
  `dosage` text NOT NULL,
  `brand_name` text NOT NULL,
  `frequency` text NOT NULL,
  `quantity/route` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `health_concern` varchar(50) NOT NULL,
  `request_datetime` datetime NOT NULL,
  `request_type` varchar(100) NOT NULL,
  `delete_flag` char(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`request_id`, `user_id`, `patient_id`, `doctor_id`, `status`, `health_concern`, `request_datetime`, `request_type`, `delete_flag`) VALUES
(1, 4, 10, 1, 'PENDING', 'Toothache', '2018-01-05 00:14:46', 'ADMIN_REQUEST', '1'),
(2, 4, 10, 1, 'PENDING', 'Toothache', '2018-01-05 05:14:46', 'ADMIN_REQUEST', '1'),
(3, 4, 10, 1, 'PENDING', 'Toothache', '2018-01-05 05:14:46', 'ADMIN_REQUEST', '1'),
(4, 4, 10, 1, 'PENDING', 'Toothache', '2018-01-05 05:14:46', 'ADMIN_REQUEST', '1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `last_login` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  `user_type_code` char(50) NOT NULL,
  `user_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `last_login`, `last_update`, `user_type_code`, `user_type_id`) VALUES
(1, 'alecka', 'alecka', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'PATIENT', 0),
(2, 'mark', 'mark', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'BHC-DOCTOR', 0),
(3, 'bryan', 'bryan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'HOSPITAL-DOCTOR', 0),
(4, 'joshua', 'joshua', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'BHC-ADMIN', 0),
(5, 'denrik', 'denrik', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'HOSPITAL-ADMIN', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_history`
--

CREATE TABLE `users_history` (
  `user_history_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `details` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_processes`
--

CREATE TABLE `user_processes` (
  `user_process_code` char(50) NOT NULL,
  `user_process_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `user_type_code` char(50) NOT NULL,
  `user_type_name` varchar(100) NOT NULL,
  `image` text NOT NULL,
  `table_name` varchar(100) NOT NULL,
  `table_key` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`user_type_code`, `user_type_name`, `image`, `table_name`, `table_key`) VALUES
('BHC-ADMIN', 'Barangay Health Center Admin', 'http://www.oprmaars.com/public/images/bhc.png', 'users', 'user_id'),
('BHC-DOCTOR', 'Barangay Health Center Doctor', 'http://www.oprmaars.com/public/images/bhcdoctor.png', 'doctors', 'doctor_id'),
('HOSPITAL-ADMIN', 'Hospital Admin', 'http://www.oprmaars.com/public/images/hsptl.png', 'users', 'user_id'),
('HOSPITAL-DOCTOR', 'Hospital Doctor', 'http://www.oprmaars.com/public/images/hosdoctor.png', 'doctors', 'doctor_id'),
('PATIENT', 'Patient', 'http://www.oprmaars.com/public/images/patient.png', 'patients', 'patient_id');

-- --------------------------------------------------------

--
-- Table structure for table `user_type_processes`
--

CREATE TABLE `user_type_processes` (
  `user_type_code` char(50) NOT NULL,
  `user_process_code` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`doctor_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`patient_id`);

--
-- Indexes for table `patients_history`
--
ALTER TABLE `patients_history`
  ADD PRIMARY KEY (`patient_history_id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`request_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `users_history`
--
ALTER TABLE `users_history`
  ADD PRIMARY KEY (`user_history_id`);

--
-- Indexes for table `user_processes`
--
ALTER TABLE `user_processes`
  ADD PRIMARY KEY (`user_process_code`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`user_type_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `doctor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `patient_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `patients_history`
--
ALTER TABLE `patients_history`
  MODIFY `patient_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users_history`
--
ALTER TABLE `users_history`
  MODIFY `user_history_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
