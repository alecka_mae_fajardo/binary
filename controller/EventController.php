<?php

class EventController
{
	public $viewTemplate;
	
	public function viewAdminPageAction()
	{
		// check if logged in
		$Session = new Session();	
		$user = $Session->getUserSession();	
		
		if (empty($user)) {
			header("Location: /user-login");
		} else {
			$username = isset($user->username) ? $user->username : '';

			//set view all patient
			$viewTemplate = $this->viewTemplate;
			
			//include the display file
			include LAYOUT;
		}
	}

	public function viewEventsAction()
	{
		$Session = new Session();	
		$user = $Session->getUserSession();	
		$username = isset($user->username) ? $user->username : '';

		$EventTable = new EventTable();
		$events = $EventTable->getEvents();

		//set view all patient
		$viewTemplate = $this->viewTemplate;
		
		//include the display file
		include LAYOUT;

	}

	public function viewEventAction()
	{
		$eventID = $_GET['patientID'];
		$EventTable = new EventTable();
		$event = $EventTable->getEvent($patientID);

		//set view all patient
		$viewTemplate = $this->viewTemplate;
		
		//include the display file
		include LAYOUT;

	}

	public function viewAddEventAction()
	{
		if (!empty($_POST)) {
			
		}
		
		//set view all patient
		$viewTemplate = $this->viewTemplate;
		
		//include the display file
		include LAYOUT;

	}
}

