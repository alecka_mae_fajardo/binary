<?php

class AdminController
{
	public $viewTemplate;
	
	public function viewAdminPageAction()
	{
		// check if logged in
		$Session = new Session();	
		$user = $Session->getUserSession();	
		
		if (empty($user)) {
			header("Location: /user-login");
		} else {
			$username = isset($user->username) ? $user->username : '';

			# To do
			// Fetch Appointments today
			$RequestTable = new RequestsTable();
			$requests = $RequestTable->getLatestRequests();

			// Available Doctors
			$DoctorTable = new DoctorTable();
			$doctorSchedule = $DoctorTable->getAvailableDoctor();

			// Count display
			$requestCount = $RequestTable->countRequests();

			$PatientTable = new PatientTable();
			$patientCount = $PatientTable->countPatients();

			//set view all patient
			$viewTemplate = $this->viewTemplate;
			
			//include the display file
			include LAYOUT;
		}
	}
}

