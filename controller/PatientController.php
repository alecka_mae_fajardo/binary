<?php

class PatientController
{
	public $viewTemplate;

	public function viewPatientPageAction()
	{
		// check if logged in
		$Session = new Session();	
		$user = $Session->getUserSession();	
		
		if (empty($user)) {
			header("Location: /user-login");
		} else {
			$username = isset($user->username) ? $user->username : '';

			//set view all patient
			$viewTemplate = $this->viewTemplate;
			
			//include the display file
			include LAYOUT;
		}
	}

	public function viewRequestPageAction()
	{
		//set view all patient
		$viewTemplate = $this->viewTemplate;
		
		//include the display file
		include LAYOUT;
	}

	public function viewMyReferralPageAction()
	{
		//set view all patient
		$viewTemplate = $this->viewTemplate;
		
		//include the display file
		include LAYOUT;
	}
	public function viewMyRecordPageAction()
	{
		//set view all patient
		$viewTemplate = $this->viewTemplate;
		
		//include the display file
		include LAYOUT;
	}
	public function viewaddPatientAction()
	{
		//set view all patient
		$viewTemplate = $this->viewTemplate;
		
		//include the display file
		include LAYOUT;
	}

	public function viewAllAction()
	{
		$Session = new Session();	
		$user = $Session->getUserSession();	
		$username = isset($user->username) ? $user->username : '';

		$PatientTable = new PatientTable();
		$patients = $PatientTable->getPatients();

		//set view all patient
		$viewTemplate = $this->viewTemplate;
		
		//include the display file
		include LAYOUT2;
	}

	public function viewAction()
	{
		$patientID = $_GET['patientID'];
		if (!empty($patientID)) {
            $PatientTable = new PatientTable();
            $patient = $PatientTable->getPatient($patientID);
        }

        $viewTemplate = $this->viewTemplate;

		include LAYOUT;
	}
	public function registerPatientAction()
	{
		$PatientTable = new PatientTable();
		$PatientTable->addPatient($_POST);

		header("Location: /view-patients");

	}
	public function viewDeletePatientAction() {
		$patientID = $_GET['patientID'];
		$PatientTable = new PatientTable();
		$patient = $PatientTable -> deletePatient($patientID);

		include LAYOUT;
	}

	public function viewEditPatientAction() 
	{
		$PatientTable = new PatientTable();
		$PatientTable->editPatient($_POST);

		header("Location: /view-patient?patientID=".$_POST['patient_id']);
		header("Location: /view-patients");
	}

	public function viewAddPrescriptionAction()
	{
		//set view all patient
		$viewTemplate = $this->viewTemplate;
		
		//include the display file
		include LAYOUT;

	}
}

/**



*/