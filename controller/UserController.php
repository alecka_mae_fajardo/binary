<?php

class UserController
{
	public $viewTemplate;

	public function indexAction()
	{
		if (empty($user)) {
			header("Location: /user-login");
		} else {
			# TO DO: redirects to landing page based on logged in user
			$view = strtolower($user->user_type_code);
			header("Location: /" . $view);		
		}

	}

	public function viewUserLoginAction()
	{

		// check if logged in
		$Session = new Session();	
		$user = $Session->getUserSession();	
		
		if (!empty($user)) 
		{
			$view = strtolower($user->user_type_code);
			header("Location: /" . $view);		
		} 
		else 
		{
			$UserTypeTable = new UserTypeTable();
			$userTypes = $UserTypeTable->getUserTypes();
			
			//set view all patient
			$viewTemplate = $this->viewTemplate;
			
			//include the display file
			include LAYOUT;
		}
	}
	
	public function loginAction()
	{
		// check if logged in
		$Session = new Session();	
		$user = $Session->getUserSession();
        $UserTypeTable = new UserTypeTable();

        if (!empty($user)) {
		    // update users table
            $result = $UserTypeTable->update(array("last_login" => "NOW()"));

			$view = strtolower($user->user_type_code);
			header("Location: /" . $view);		
		} else {
			$user_type = $_GET['user_type'];

			if ($user_type) { 
				$userType = $UserTypeTable->getUserType(strtoupper($user_type));
				
				if (!$userType)
					header("Location: /user-login");
				else {
					if (isset($_GET['msg'])) {
						$error = "Incorrect login details";
					}

					$viewTemplate = $this->viewTemplate;

					//include the display file
					include LAYOUT;
				}

			} else {
				header("Location: /user-login");
			}
		}
	}

	public function validateLoginAction()
	{
		$params = $_POST;

		$UserTable = new UserTable();
		$user = $UserTable->getUserLogin($params['user_type_code'], $params['username'], $params['password']);
		if ($user) {
			// set in session
			$Session = new Session();
			$Session->setUserSession($user);

			// redirect to dashboard
			$view = strtolower($user->user_type_code);
			header("Location: /" . $view);			

		} else {
			header("Location: /login?user_type=" . $params['user_type_code'] . '&msg=1');
		}
	}

	public function logoutAction()
	{
		$Session = new Session();
		$Session->unsetSession();

		header("Location: /user-login");
	}
}