<?php

class DoctorController
{
	public $viewTemplate;
	
	public function viewDoctorPageAction()
	{
		// check if logged in
		$Session = new Session();	
		$user = $Session->getUserSession();	
		
		if (empty($user)) {
			header("Location: /user-login");
		} else {
			$username = isset($user->username) ? $user->username : '';

			//set view all patient
			$viewTemplate = $this->viewTemplate;
			
			//include the display file
			include LAYOUT;
		}
	}

	public function viewAction()
	{
		$doctorID = $_GET['doctorID'];
		if (!empty($doctorID)) {
            $DoctorTable = new DoctorTable();
            $doctor = $DoctorTable->getDoctor($doctorID);
        }

        $viewTemplate = $this->viewTemplate;

		include LAYOUT;
	}

	public function viewAllAction()
	{
		$Session = new Session();	
		$user = $Session->getUserSession();	
		$username = isset($user->username) ? $user->username : '';

		$DoctorTable = new DoctorTable();
		$doctors = $DoctorTable->getDoctors();

		//set view all patient
		$viewTemplate = $this->viewTemplate;

		//include the display file
		include LAYOUT2;
	}

	public function viewaddDoctorAction()
	{
		//set view all patient
		$viewTemplate = $this->viewTemplate;
		
		//include the display file
		include LAYOUT;
	}

	public function registerDoctorAction()
	{
		$DoctorTable = new DoctorTable();
		$DoctorTable->addDoctor($_POST);

		header("Location: /view-doctors");

	}

	public function UpdateDoctorAction() 
	{
		$DoctorTable = new DoctorTable();
		$DoctorTable->editDoctor($_POST);

		header("Location: /view-doctors?doctorID=".$_POST['doctor_id']);
	}

	public function deleteDoctorAction()
    {
		$doctorID = $_GET['doctorID'];
		$DoctorTable = new DoctorTable();
		$doctor = $DoctorTable -> deleteDoctor($doctorID);

		header("Location: /view-doctors");
	}

	public function showAppointmentsAction()
    {
        $Session = new Session();
        $user = $Session->getUserSession();
        $username = isset($user->username) ? $user->username : '';

        $RequestTable = new RequestsTable();
        $requests = $RequestTable->getRequests();

        //set view all patient
        $viewTemplate = $this->viewTemplate;

        //include the display file
        include LAYOUT2;
    }
}
