<?php

class RequestController
{
	public $viewTemplate;
	
	public function viewRequestsAction()
	{
		$Session = new Session();	
		$user = $Session->getUserSession();	
		$username = isset($user->username) ? $user->username : '';

		$RequestTable = new RequestsTable();
		$requests = $RequestTable->getRequests();

		//set view all patient
		$viewTemplate = $this->viewTemplate;
		
		//include the display file
		include LAYOUT2;
	}

	public function viewRequestAction()
	{
		$requestID = $_GET['requestID'];
		$RequestTable = new RequestsTable();
		$request = $RequestTable->getRequest($requestID);

		$viewTemplate = "view/request/view-one.phtml";
		include LAYOUT;
	}

	public function addRequestAction()
	{
        $Session = new Session();
        $user = $Session->getUserSession();
        $username = isset($user->username) ? $user->username : '';

        $PatientTable = new PatientTable();
        $patients = $PatientTable->getPatients();

        $DoctorTable = new DoctorTable();
        $doctors = $DoctorTable->getDoctors();

        //set view all patient
		$viewTemplate = $this->viewTemplate;
		
		//include the display file
		include LAYOUT;

	}

    public function processRequestAction()
    {
        $Session = new Session();
        $user = $Session->getUserSession();

        $params = array(
            'request_id'    => NULL,
            'user_id' => (int)$user->user_id,
            'patient_id'    => (int)$_POST['patient_id'],
            'doctor_id'    => (int)$_POST['doctor_id'],
            'status' => "PENDING",
            'health_concern'    => $_POST['health_concern'],
            'request_datetime' => $_POST['request_date'] . ' ' . $_POST['request_time'],
            'request_type' => in_array($user->user_type_code, array("BHC-ADMIN", "HOSPITAL-ADMIN")) ? "ADMIN_REQUEST" : "DOCTOR_REQUEST",
            'delete_flag'    => 0,
        );

        $RequestTable = new RequestsTable();
        $result = $RequestTable->addRequest($params);

        header("Location: /view-requests");
    }
}